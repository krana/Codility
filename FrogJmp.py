def solution(X, Y, D):
    
    N=(Y-X)/float(D)
    if int(N)!=N:
        N=int(N)+1
    
    return int(N)