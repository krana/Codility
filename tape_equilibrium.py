def solution(A):
    for P in range(1,len(A)):
        ant=0
        suc=0
        i=0
        while i < P:
            ant+=A[i]
            i+=1
            
        while i>=P and i<len(A):
            suc+=A[i]
            i+=1
        if P==1:
            diff=abs(ant-suc)
        else:
            diff=min(diff,abs(ant-suc))
#        print diff
#        print abs(ant-suc)    
#        diff=min(diff,abs(ant-suc))
    
    return diff
    # write your code in Python 2.7
    #pass